import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Modal,
  Pressable, TextInput
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const SelectAdress = () => {
  const [modalVisible, setModalVisible] = useState(true);
  const [address, setAddress] = useState("");
  const navigation = useNavigation();
  const route = useRoute();
  let booking = route ? route.params.bookingData : '';
  const handleSubmit=()=>{
    navigation.navigate("BookingSlotes",{bookingData:booking})
  }
  return (
    <SafeAreaView style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Pressable onPress={() => setModalVisible(!modalVisible)}>
              <Image
                source={require('../assets/img/closeicon.png')}
                style={styles.closeicon}
              />
            </Pressable>
            <Text style={styles.text22}> Saved Address </Text>

            <View style={styles.additemcard}>
              <View style={{flex: 1}}>
                <TouchableOpacity>
                  <MaterialIcons
                    name="add"
                    size={25}
                    color="#0873BA"
                    style={{marginTop: 5}}
                  />
                </TouchableOpacity>
              </View>

              <View style={{flex: 9}}>
                <Text style={styles.text2222}>Add Address </Text>
              </View>
            </View>

            <View style={styles.booknowcard}>
              <View style={{flex: 2}}>
                <MaterialCommunityIcons
                  name="checkbox-blank-circle"
                  size={25}
                  color="#0873BA"
                  style={{marginTop: 5}}
                />
              </View>

              <View style={{flex: 8}}>
                <Text style={styles.text33}>Home </Text>
                <Text style={styles.text333}>
                <TextInput
                  placeholder="Address"
                  placeholderTextColor="gray"
                  style={styles.textinput1}
                  onChangeText={address => setAdress(address)}
                  />
                </Text>
              </View>
            </View>

            <TouchableOpacity style={styles.btn1} onPress={handleSubmit}>
              <Text style={styles.text3}>BOOK SLOT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* <View style={{justifyContent: 'center', marginTop: 300}}>
        <Pressable
          style={[styles.button, styles.buttonOpen]}
          onPress={() => setModalVisible(true)}>
          <Text>Hello</Text>
        </Pressable>
      </View> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  mainContainer: {
    padding: 10,
    flexDirection: 'row',
    // borderWidth:1,
    // borderColor:"#D3D3D3",
    // backgroundColor:"#F3F3F3",
    // elevation:5,
    // borderRadius:10
  },
  btn: {
    marginTop: 10,
    backgroundColor: '#0873BA',
    height: 40,
    width: 70,
    borderRadius: 5,
    fontFamily: 'Poppins',
  },
  text1: {
    margin: 5,
  },
  btntext: {
    alignSelf: 'center',
    color: 'white',
    marginTop: 10,
    fontSize: 13,
    fontFamily: 'Poppins',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: 'rgba(0, 0, 0, .7)',
  },
  modalView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    height: '52%',
    width: '90%',
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.7,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  closeicon: {
    alignSelf: 'flex-end',
    marginLeft: '100%',
  },

  text22: {
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginTop: 20,
    color: 'black',
  },

  text222: {
    fontSize: 15,
    paddingTop: 10,
    alignSelf: 'flex-start',
  },

  text2222: {
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    padding: 5,
    color: '#0873BA',
    marginLeft: 20,
  },

  additemcard: {
    flexDirection: 'row',
    padding: 5,
    borderColor: 'gray',
    marginTop: 20,
    alignSelf: 'flex-start',
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 1,
  },

  booknowcard: {
    flexDirection: 'row',
    Height: 30,
    width: '100%',
    padding: 5,
    marginTop: 20,
  },

  btn1: {
    marginTop: 10,
    backgroundColor: '#0873BA',
    height: '16%',
    width: '100%',
    borderRadius: 5,
    fontFamily: 'Poppins',
  },

  text3: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 15,
  },

  text33: {
    fontSize: 18,
    fontWeight: 'bold',

    marginTop: 5,
  },

  text333: {
    fontSize: 14,
    fontWeight: '500',

    marginTop: 10,
  },
});

export default SelectAdress;
