import {
  View,
  Text,
  StyleSheet,
  Image,
  Button,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import React,{useEffect} from 'react';
import {OrderList} from "../Api/Api"
import AUTH_CONFIG from '../Constant/Constant';
import {useNavigation} from '@react-navigation/native';
import AnimatedLoader from 'react-native-animated-loader';
import AsyncStorage from '@react-native-async-storage/async-storage';
const BookingTab = () => {
  const navigation = useNavigation();
  const [data, setData] = React.useState() 
  const [userid, setUserId] = React.useState() 
  const [visible, setVisible]= React.useState(false) 
  useEffect(()=>{
    AsyncStorage.getItem('user_id').then((res)=>{  setUserId(res)  })

  })
  useEffect(() => {

    OrderList(userid)
      .then(results => {
        setData(results.data);
        setVisible(true)
      })
      .catch(error => {
        console.warn('Error');
      });

  },[userid]);
  
  return (
    <ScrollView>
      <ImageBackground
        source={require('../../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 6}}>
            <Text style={styles.HeaderTitile}> MY Bookings</Text>
          </View>
          <View style={{flex: 2}}>
            <Image
              source={require('../../assets/img/sidebar.png')}
              style={styles.imgheader2}
            />
          </View>
        </View>
      </ImageBackground>
      {/* <AnimatedLoader
      visible={visible}
      overlayColor="rgba(255,255,255,0.75)"
      animationStyle={styles.lottie}
      speed={1}>
      <Text>Doing something...</Text>
    </AnimatedLoader> */}
      {data && data.map((item,index)=>{
        return(
          <View style={styles.mainContainer} key={index}>
          <View style={{flex: 1.5}}>
            <Image source={require('../../assets/img/acrepair.png')} />
          </View>
          <View style={styles.textcontainer}>
            <Text style={styles.textone}>AC SERVICES </Text>
            <Text style={{color: 'gray'}}>Order Number</Text>
            <Text style={{color: 'gray'}}>#{item.id}</Text>
            <Text style={styles.texttwo}>Rs. {item.amount}</Text>
            <Text style={styles.texttwo}>
              Booked for-{item.booking_date}, Time {item.booking_time}
            </Text>
          </View>
          <View style={{flex: 3}}>
            <TouchableOpacity style={styles.Buttonconatiner}>
              <Text
                style={{
                  fontSize: 13,
                  color: '#0873BA',
                  marginTop: 5,
                  alignSelf: 'center',
                }}>
                {' '}
                View Details
              </Text>
            </TouchableOpacity>
          </View>
        </View>  
        )
      })}
      
      
    </ScrollView>
  );
};

export default BookingTab;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  mainContainer: {
    flexDirection: 'row',
    padding: 20,
    position: 'relative',
    backgroundColor: 'white',
  },
  textcontainer: {
    marginLeft: 15,
    flex: 5,
  },
  Buttonconatiner: {
    borderWidth: 1,
    borderColor: '#0873BA',
    height: 30,
    width: 100,
    borderRadius: 5,
  },
  textone: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
  texttwo: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 12,
    color: '#000000',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {
    alignSelf: 'center',
    marginTop: 30,
  },
  imgheader2: {
    alignSelf: 'flex-start',
    marginTop: 30,
    marginRight: 20,
  },
  lottie: {
    width: 100,
    height: 100
  }
});
