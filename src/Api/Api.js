import axios from 'axios';
import AUTH_CONFIG from "../Constant/Constant";

export function userLogin(email, password) {
	console.log("emailss",email)
	return axios.post(AUTH_CONFIG.BASE_URL + 'api/users/login', { "email": email, "password": password }).then(res => res.data)
}

export function customerRegister(data) {
	return axios.post(AUTH_CONFIG.BASE_URL + 'api/users/add', data);
}

export function updateOtp(email) {
	return axios.post(AUTH_CONFIG.BASE_URL + 'api/users/updateOtp', {"email":email});
}

export function verifyOtp(email,otp) {
	return axios.post(AUTH_CONFIG.BASE_URL + 'api/users/verifyOtp', {"email":email,"otp":otp});
}

export function addBooking(data) {
	return axios.post(AUTH_CONFIG.BASE_URL + 'api/booking/add', {"user_id":data.user_id,"id":data.id,"price":data.price,"btime":data.btime,"bdate":data.bdate});
}

// export function advisorRegister(data) {
// 	return axios.post(AUTH_CONFIG.BASE_URL + 'api/v1/admin/auth/registerAdvisor', data);
// }

export function catList() {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/category/get').then(res => res.data)
}
export function SubCatList(id) {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/category/get_child/'+id).then(res => res.data)
}

export function BannerList() {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/banner/get').then(res => res.data)
}
export function ServiceList() {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/services/get').then(res => res.data)
}
export function Service(id) {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/services/get/'+id).then(res => res.data)
}
export function ServiceByCat(cat) {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/services/get_by_cat/'+cat).then(res => res.data)
}
export function OrderList(user_id) {  
	return axios.get(AUTH_CONFIG.BASE_URL +'api/booking/get/'+user_id).then(res => res.data)
}