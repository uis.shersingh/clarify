import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  SafeAreaView,
  Button,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';

const Data = [
  {
    id: '1',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },

  {
    id: '2',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },

  {
    id: '3',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },

  {
    id: '4',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },

  {
    id: '5',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },

  {
    id: '6',
    img: require('../../assets/img/Notificationprofile.png'),
    img1: require('../../assets/img/2.png'),
    Title1: 'SALE IS LIVE',
    Title2:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur adipiscing elit ',
    Title3: '1m ago.',
  },
];

export default NotificationTab = () => {
  const renderItem = ({item}) => (
    <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
      <View style={{flex: 2}}>
        <Image source={item.img} style={styles.img} />
        {/* <Image source={item.img1} style={styles.img1} /> */}
      </View>
      <View style={{flex: 5}}>
        <Text style={styles.title1}>{item.Title1}</Text>
        <Text style={styles.title2}>{item.Title2}</Text>
      </View>
      <View style={{flex: 3}}>
        <Text style={styles.title3}>{item.Title3}</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.maincontainer}>
      <ImageBackground
        source={require('../../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 6}}>
            <Text style={styles.HeaderTitile}>Notification</Text>
          </View>
          <View style={{flex: 2}}>
            <Image
              source={require('../../assets/img/sidebar.png')}
              style={styles.imgheader2}
            />
          </View>
        </View>
      </ImageBackground>

      <FlatList
        style={{margin: 10}}
        data={Data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title1: {
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 16,
    marginTop: 10,
    color: '#000000',
  },

  title2: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 14,
    color: '#7B6F72',
  },

  title3: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 14,
    color: '#7B6F72',
    textAlign: 'center',
    marginTop: 10,
  },
  img: {
    alignSelf: 'center',
    marginTop: 15,
  },

  img1: {
    alignSelf: 'center',
    marginTop: 45,
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {
    alignSelf: 'center',
    marginTop: 30,
  },

  imgheader2: {
    alignSelf: 'flex-start',
    marginTop: 30,
    marginRight: 20,
  },
});
