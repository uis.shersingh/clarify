import AsyncStorage from '@react-native-async-storage/async-storage';

export async function  userEmail() {
    let user = await AsyncStorage.getItem('user_email'); 
    let parsed = JSON.parse(user); 
    return  parsed;
}  

export const  current_user_id=async ()=>{
    let user = await AsyncStorage.getItem('user_id'); 
    let parsed = JSON.parse(user);   
    return  parsed;  
}    
