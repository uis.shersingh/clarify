import {View, Text, StyleSheet, Image, Button, ScrollView} from 'react-native';
import React from 'react';

const Orderlist = () => {
  return (
    <ScrollView style={styles.Container}>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
      <View style={styles.mainContainer}>
        <View>
          <Image source={require('../assets/img/acrepair.png')} />
        </View>
        <View style={styles.textcontainer}>
          <Text style={styles.textone}>AC SERVICES</Text>
          <Text>Order Number</Text>
          <Text>CAR-120789</Text>
          <Text style={styles.texttwo}>Rs. 2599</Text>
          <Text style={styles.texttwo}>
            Booked for-26.09.2022, Time 09AM-10AM
          </Text>
        </View>
        <View style={styles.Buttonconatiner}>
          <Button title="VIEW DETAILS" />
        </View>
      </View>
    </ScrollView>
  );
};

export default Orderlist;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  mainContainer: {
    flexDirection: 'row',
    padding: 20,
    position: 'relative',
  },
  textcontainer: {
    marginLeft: 15,
  },
  Buttonconatiner: {
    position: 'absolute',
    right: 15,
    top: 25,
  },
  textone: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
  },
  texttwo: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 12,
    color: '#000000',
  },
});
