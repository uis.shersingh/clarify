import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';

const OTP2 =()=> {
  const navigation = useNavigation();

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Text style={styles.title1}>Enter verification code</Text>
      <Text style={styles.title2}>
        We have sent you a 4 digit verification code on
      </Text>
      <Text style={styles.title3}>+91 9871279995</Text>

      <View
        style={{
          height: hp('7%'),
          width: wp('85% '),
          justifyContent: 'center',
          alignSelf: 'center',
          flexDirection: 'row',

          marginTop: hp('5%'),
        }}>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
      </View>
      <Text style={styles.title4}>0:26</Text>
      <TouchableOpacity
        style={styles.btncontainer}
        onPress={() => navigation.navigate('BottomTab')}>
        <Text style={styles.title5}>Login/Signup</Text>
      </TouchableOpacity>
    </View>
  );
};

export default OTP2;

const styles = StyleSheet.create({
  title1: {
    fontFamily: 'Poppins',
    fontWeight: '600',
    fontStyle: 'normal',
    fontSize: 22,
    marginTop: 60,
    color: '#000000',
    alignSelf: 'center',
  },

  title2: {
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    alignSelf: 'center',
    fontSize: 14,
    color: '#7B6F72',
    marginTop: 8,
  },

  title3: {
    fontFamily: 'Poppins-Medium',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 18,
    marginTop: 20,
    color: '#000000',
    alignSelf: 'center',
  },

  title4: {
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 18,
    marginTop: 4,
    color: '#F0350C',
    alignSelf: 'center',
  },

  title5: {
    fontFamily: 'Poppins-SemiBold',
    fontWeight: '600',

    fontSize: 16,
    marginTop: 10,
    color: 'white',
    alignSelf: 'center',
  },

  btncontainer: {
    height: 50,
    width: 300,
    borderColor: '#0873BA',
    borderWidth: 1.3,
    alignSelf: 'center',
    marginTop: 30,
    borderRadius: 5,
    backgroundColor: '#0873BA',
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '600',
    fontSize: 16,
    alignSelf: 'flex-start',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 32},

  TextInput: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderWidth: 1.3,
    borderColor: 'gray',
    alignSelf: 'center',
    borderRadius: 5,
  },
});
