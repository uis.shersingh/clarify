import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet,Image, Text, StatusBar } from 'react-native';

const DATA = [
  {
    id: '1',
      img: require('../../assets/img/one.png'),
      productHeading: 'Service',
      Price: 'Rs.99',
  },
  {
    id: '2',
      img: require('../../assets/img/Two.png'),
      productHeading: 'Gas Services',
      Price: 'Rs.99',
  },
  {
    id: '3',
    img: require('../../assets/img/three.png'),
    productHeading: 'Installation',
    Price: 'Rs.99',
  },
];

const Item = ({ title , productHeading }) => (
    <View style={styles.item}>
    <Text style={styles.title}>{productHeading}</Text>
  </View>
);

const FrequenltyItem = () => {
  const renderItem = ({ item }) => (


     <View style={styles.item}>
        <Image source={item.img}  />
    <Text style={styles.title}>{item.productHeading}</Text>
    <Text style={styles.title}>{item.Price}</Text>
  </View>
  );

  return (
    <SafeAreaView style={styles.container}>
<View style={{borderColor:"#D3D3D3" , borderWidth:1 }}>   
<Text style={styles.title1}>Frequently added together </Text>
      <FlatList horizontal={true} 
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    
    padding: 5,
    marginVertical: 8,
    marginHorizontal: 16,
    
  },
  title: {
    fontSize: 15,
    fontWeight:"bold",
    marginTop:3
  },
  title1: {
    fontSize: 18,
    fontWeight:"500",
    margin:8,
    color:"gray"

  },
});

export default FrequenltyItem;