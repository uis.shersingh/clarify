import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

import {useNavigation} from '@react-navigation/native';

const ProfileEdit = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.Container}>
      <Image
        source={require('../assets/img/background.png')}
        style={{alignSelf: 'center', width: '100%', height: '25%'}}
      />
      <View style={{flexDirection: 'row', marginTop: -180}}>
        <View style={{flex: 2}}>
          <TouchableOpacity onPress={() => navigation.navigate('UserTab')}>
            <Image
              source={require('../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 8}}>
          <Text style={styles.HeaderTitile}>Edit Profile</Text>
        </View>
      </View>

      <Image
        source={require('../assets/img/profilepic.png')}
        style={{alignSelf: 'center', marginTop: 40}}
      />

      <Text
        style={{
          alignSelf: 'center',
          fontFamily: 'Poppins',
          fontStyle: 'normal',
          fontSize: 14,
          color: '#000000',
        }}>
        Change Picture
      </Text>
      <View style={styles.main}>
        <Text style={styles.Inputtext}>Username</Text>
        <TextInput style={styles.inputfield} placeholder="John Doe" />
        <Text style={styles.Inputtext}>Email I’d</Text>
        <TextInput style={styles.inputfield} placeholder="John Doe@gmail.com" />
        <Text style={styles.Inputtext}>Phone Number</Text>
        <TextInput style={styles.inputfield} placeholder="9871270000" />
        <View style={{marginTop: 20, margin: 10}}>
          <Button title="UPDATE" style={{borderRadius: 20}} />
        </View>
      </View>
    </View>
  );
};

export default ProfileEdit;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
  },
  main: {
    padding: 10,
    marginTop: 10,
  },
  Inputtext: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#000000',
    marginTop: 10,
    margin: 10,
  },
  inputfield: {
    borderWidth: 1,
    // borderBottomWidth:0.5,
    // borderStyle: 'solid',
    borderColor: 'gray',
    width: '95%',
    alignSelf: 'center',
    // borderRadius:3,
    borderRadius: 5,
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 80,
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 30},
});
