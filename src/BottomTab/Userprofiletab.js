import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

const Myprofile = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.profileContainer}>
      <Image
        source={require('../../assets/img/background.png')}
        style={{alignSelf: 'center', width: '100%', height: '28%'}}
      />
      <View style={{flexDirection: 'row', marginTop: -180}}>
        <View style={{flex: 2}}>
          <Image
            source={require('../../assets/img/arrow.png')}
            style={styles.imgheader}
          />
        </View>
        <View style={{flex: 8}}>
          <Text style={styles.HeaderTitile}>My Profile</Text>
        </View>
      </View>

      <Image
        source={require('../../assets/img/blank.png')}
        style={{alignSelf: 'center', marginTop: 30}}
      />
      <Image
        source={require('../../assets/img/addphoto.png')}
        style={{alignSelf: 'center', marginTop: 10}}
      />
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#FFF4E4',
          elevation: 10,
          marginTop: 20,
          padding: 10,
        }}>
        <View style={{flex: 8}}>
          <Text style={styles.texone}>John Doe</Text>
        </View>
        <View style={{flex: 2}}>
          <TouchableOpacity onPress={() => navigation.navigate('ProfileEdit')}>
            <Image
              source={require('../../assets/img/pencil.png')}
              style={{marginLeft: 'auto'}}
            />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <View style={styles.MainContent}>
          <View style={{flexDirection: 'row'}}>
            <Image source={require('../../assets/img/Vectorone.png')} />
            <Text style={styles.styledtext}>Help Centre</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectortwo.png')} />
            <Text style={styles.styledtext}>My Bookings</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectorthree.png')} />
            <Text style={styles.styledtext}>Manage Address</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectorfour.png')} />
            <Text style={styles.styledtext}>Refer and Earn</Text>
          </View>

          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectorsix.png')} />
            <Text style={styles.styledtext}>About Us</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectorseven.png')} />
            <Text style={styles.styledtext}>Rate Carify</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/Vectoreight.png')} />
            <Text style={styles.styledtext}>Account Settings</Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 30}}>
            <Image source={require('../../assets/img/logout.png')} />
            <Text style={styles.styledtext}>Logout</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Myprofile;

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 80,
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 30},
  texone: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
    marginLeft: 15,
  },
  styledtext: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 13,
    color: 'rgba(42, 45, 55, 0.6)',
    alignSelf: 'center',
    marginLeft: 10,
  },
  MainContent: {
    padding: 20,
  },
});
