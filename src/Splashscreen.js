import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';

const SplashScreen = () => {
  const navigation = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      // go to Home page
      navigation.navigate('WelcomeScreen');
    }, 3000);
  }, []);
  return (
    <View>
      <ImageBackground
        source={require('../assets/img/splash1.png')}
        style={{height: '100%', width: '100%'}}
      />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  text1: {
    fontSize: 18,
    color: '#7B6F72',
    fontFamily: 'Poppins-MediumItalic',
    textAlign: 'center',
    marginTop: -40,
    fontWeight: '500',
  },
});
