import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

const HomeScreenscrollone = () => {
  const navigation = useNavigation();

  const AcServicesData = [
    {
      id: '1',
      img: require('../assets/img/arms.png'),
      productHeading: 'Full arms + Underarms',
      Price: 'Rs.599',
    },
    {
      id: '2',
      img: require('../assets/img/facial.png'),
      productHeading: 'Instant Glow Facial',
      Price: 'Rs.649',
    },
    {
      id: '3',
      img: require('../assets/img/arms.png'),
      productHeading: 'Manicure and Nail Art',
      Price: 'Rs. 2249',
    },
    {
      id: '4',
      img: require('../assets/img/manicure.png'),
      productHeading: 'Manicure and Nail Art',
      Price: 'Rs.1349',
    },
  ];
  const ApplianceData = [
    {
      id: '1',
      img: require('../assets/img/laptop.png'),
      productHeading: 'Laptop Repairs',
      Price: 'Rs.599',
    },
    {
      id: '2',
      img: require('../assets/img/fridge.png'),
      productHeading: 'Refigrator Services & Repair',
      Price: 'Rs.649',
    },
    {
      id: '3',
      img: require('../assets/img/laptop.png'),
      productHeading: 'Air Conditioner Service & Repair',
      Price: 'Rs. 2249',
    },
    {
      id: '4',
      img: require('../assets/img/manicure.png'),
      productHeading: 'Refigrator Services & Repair',
      Price: 'Rs.1349',
    },
  ];

  const CleaningServicesData = [
    {
      id: '1',
      img: require('../assets/img/pest.png'),
      productHeading: 'Pest Control',
      Price: 'Rs.599',
    },
    {
      id: '2',
      img: require('../assets/img/bucket.png'),
      productHeading: 'Home Cleaning',
      Price: 'Rs.649',
    },
    {
      id: '3',
      img: require('../assets/img/cloth.png'),
      productHeading: 'Dry Cleaning ',
      Price: 'Rs. 2249',
    },
    {
      id: '4',
      img: require('../assets/img/bucket.png'),
      productHeading: 'Home Cleaning',
      Price: 'Rs.1349',
    },
  ];
  const PaintingServicesData = [
    {
      id: '1',
      img: require('../assets/img/paint.png'),
      productHeading: 'Full House Painting',
      Price: 'Rs.599',
    },
    {
      id: '2',
      img: require('../assets/img/home.png'),
      productHeading: 'Home repair work',
      Price: 'Rs.649',
    },
    {
      id: '3',
      img: require('../assets/img/CARD.png'),
      productHeading: 'Fabrication work',
      Price: 'Rs. 2249',
    },
    {
      id: '4',
      img: require('../assets/img/bucket.png'),
      productHeading: 'df',
      Price: 'Rs.1349',
    },
  ];
  const AddServicesData = [
    {
      id: '1',
      img: require('../assets/img/females.png'),
      productHeading: 'Maid Services',
      Price: 'Rs.599',
    },
    {
      id: '2',
      img: require('../assets/img/toys.png'),
      productHeading: 'Party Decoration',
      Price: 'Rs.649',
    },
    {
      id: '3',
      img: require('../assets/img/splash.png'),
      productHeading: 'Tution Classes',
      Price: 'Rs. 2249',
    },
    {
      id: '4',
      img: require('../assets/img/toys.png'),
      productHeading: 'Party Decoration',
      Price: 'Rs.1349',
    },
  ];
  return (
    <View style={styles.Container}>
      <View>
        <Text style={styles.TextOne}>Top Picks for you</Text>
        <Text style={styles.TextTwo}>Salon Services For Women & Men</Text>
      </View>
      <View>
        <FlatList
          data={AcServicesData}
          horizontal={true}
          renderItem={ServiceData => {
            return (
              <View>
                <View style={{margin: 10}}>
                  <Image source={ServiceData.item.img} />
                </View>
                <View style={{}}>
                  <Text style={styles.textheading}>
                    {ServiceData.item.productHeading}
                  </Text>
                  <Text style={styles.productprice}>
                    {ServiceData.item.Price}
                  </Text>
                </View>
              </View>
            );
          }}
        />
      </View>
      <View style={{marginTop: 25}}>
        <TouchableOpacity
          // onPress={() => navigation.navigate('ServiceCategory')}
          onPress={() => navigation.navigate('SaloonTabView')}
          style={{borderWidth: 1.3, borderColor: 'blue', borderRadius: 10}}>
          <Text style={styles.btntext}>View All</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.TextTwo}>Appliance Repair</Text>
      </View>
      <View>
        <FlatList
          data={ApplianceData}
          horizontal={true}
          renderItem={ServiceData => {
            return (
              <View>
                <View style={{margin: 10}}>
                  <Image source={ServiceData.item.img} />
                </View>
                <View style={{}}>
                  <Text style={styles.textheading}>
                    {ServiceData.item.productHeading}
                  </Text>
                  <Text style={styles.productprice}>
                    {ServiceData.item.Price}
                  </Text>
                </View>
              </View>
            );
          }}
        />
      </View>
      <View style={{marginTop: 25}}>
        <TouchableOpacity
          style={{borderWidth: 1, borderColor: 'blue', borderRadius: 10}}
          onPress={() => navigation.navigate('AcServices')}>
          <Text style={styles.btntext}>View All</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.TextTwo}>Cleaning Services and Pest Control </Text>
      </View>
      <View>
        <FlatList
          data={CleaningServicesData}
          horizontal={true}
          renderItem={ServiceData => {
            return (
              <View>
                <View style={{margin: 10}}>
                  <Image source={ServiceData.item.img} />
                </View>
                <View style={{}}>
                  <Text style={styles.textheading}>
                    {ServiceData.item.productHeading}
                  </Text>
                  <Text style={styles.productprice}>
                    {ServiceData.item.Price}
                  </Text>
                </View>
              </View>
            );
          }}
        />
      </View>
      <View style={{marginTop: 25}}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ServiceCategory')}
          style={{borderWidth: 1, borderColor: 'blue', borderRadius: 10}}>
          <Text style={styles.btntext}>View All</Text>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.TextTwo}>Painting Services and Repairs</Text>
      </View>
      <View>
        <FlatList
          data={PaintingServicesData}
          horizontal={true}
          renderItem={ServiceData => {
            return (
              <View>
                <View style={{margin: 10}}>
                  <Image source={ServiceData.item.img} />
                </View>
                <View style={{}}>
                  <Text style={styles.textheading}>
                    {ServiceData.item.productHeading}
                  </Text>
                  <Text style={styles.productprice}>
                    {ServiceData.item.Price}
                  </Text>
                </View>
              </View>
            );
          }}
        />
      </View>

      <View style={{marginTop: 40}}>
        <Text style={styles.headingtext}>Best Home Services near you</Text>
      </View>
      <View style={{marginTop: 15}}>
        <View style={{flexDirection: 'row'}}>
          <Image source={require('../assets/img/Iconone.png')} />
          <Text style={styles.servicetext}>Secured and verified Services</Text>
        </View>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Image source={require('../assets/img/icontwo.png')} />
          <Text style={styles.servicetext}>Trained and verified Staff</Text>
        </View>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <Image source={require('../assets/img/iconthree.png')} />
          <Text style={styles.servicetext}>Safe and hygenic services</Text>
        </View>
      </View>
    </View>
  );
};

export default HomeScreenscrollone;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
  },
  TextOne: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#000000',
  },
  TextTwo: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000000',
    marginTop: 30,
  },
  textheading: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 13,
    color: '#7B6F72',
  },
  productprice: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 12,
    color: '#000000',
  },
  btntext: {
    alignSelf: 'center',
    color: 'blue',
    padding: 10,
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#0873BA',
  },
  headingtext: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 20,
    color: '#000000',
  },
  servicetext: {
    fontFamily: 'Poppins',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 12,
    color: '#7B6F72',
    marginLeft: 10,
  },
});
