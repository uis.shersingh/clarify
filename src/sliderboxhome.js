import React,{useState,useEffect} from 'react';
import {StyleSheet, View, Text, Dimensions, Image} from 'react-native';
import ViewSlider from 'react-native-view-slider';
import {BannerList} from "./Api/Api"
import AUTH_CONFIG from './Constant/Constant';

const {width, height} = Dimensions.get('window');

function SliderBoxHome() {
  const [data, setData] = useState();
  useEffect(() => {
    BannerList()
      .then(results => {
        setData(results.data);
      })
      .catch(error => {
        console.warn('Error');
      });

  }, []);
  return (
    <>
      <ViewSlider
        renderSlides={
          <>
           {/* {data &&
            data.map((item, index) => {
              let img_url=`${AUTH_CONFIG.BASE_URL}/public/assets/images/upload/banner/${item.brochure}`;
              return(
            <View style={styles.viewBox} key={index}>
              <Image
                      source={{ uri:img_url}}
                      style={{marginRight: 40, borderRadius: 20}}
              />
            </View>
              )
            })} */}
            <View style={styles.viewBox}>
              <Image
                source={require('../assets/img/sliderhome2.png')}
                style={{marginRight: 40, borderRadius: 20}}
              />
            </View>
            <View style={styles.viewBox}>
              <Image
                source={require('../assets/img/sliderhome3.png')}
                style={{marginRight: 40, borderRadius: 20}}
              />
            </View>
            <View style={styles.viewBox}>
              <Image
                source={require('../assets/img/sliderhome4.png')}
                style={{marginRight: 40, borderRadius: 20}}
              />
            </View> 
          </>
        }
        style={styles.slider} //Main slider container style
        height={200} //Height of your slider
        slideCount={4} //How many views you are adding to slide
        dots={true} // Pagination dots visibility true for visibile
        dotActiveColor="red" //Pagination dot active color
        dotInactiveColor="gray" // Pagination do inactive color
        dotsContainerStyle={styles.dotContainer} // Container style of the pagination dots
        autoSlide={true} //The views will slide automatically
        slideInterval={5000} //In Miliseconds
      />
    </>
  );
}

const styles = StyleSheet.create({
  viewBox: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    width: width,
    padding: 10,
    alignItems: 'center',
    height: 150,
  },
  slider: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dotContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 15,
  },
});

export default SliderBoxHome;
