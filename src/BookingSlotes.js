import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Pressable, Alert
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation,useRoute} from '@react-navigation/native';
import {addBooking} from './Api/Api';
import {current_user_id} from "./Common"
const BookingSlotes = () => {
  const [bdate,setBdate]=useState("");
  const [btime,setBtime]=useState("");
  const [userid,setUserId]=useState("");
  const navigation = useNavigation();
  const route = useRoute();
  let booking = route ? route.params.bookingData : '';
  useEffect(()=>{
    (async function (){
      const user_id=await current_user_id()
      if(!user_id){
        navigation.navigate("Login1")
      } else {
        setUserId(user_id)
      }
    })()
  })
  const array = [
    '9PM',
    '10PM',
    '11PM',
    '12PM',
    '1AM',
    '2AM',
    '3AM',
    '4AM',
    '5AM',
    '6AM',
  ];

  const date1 = ['28 sep', '29 sep', '30 sep'];

  // var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();

  //Alert.alert(date + '-' + month + '-' + year);
  // You can turn it in to your desired format

  const date = new Date(); // 2009-11-10
  const month = date.toLocaleString('default', {month: 'long'});
  console.log(month);
  const handleDate=()=>{
    setBdate(date);
  }
  const handleTime=(item)=>{
    setBtime(item);
  }
  const submitData=()=>{
    //navigation.navigate("Address",{bookingData:booking,B_date:bdate,B_time:btime})
    if(bdate===""){
      Alert.alert("Please select date");
    }
    else if(btime===""){
      Alert.alert("Please select time");
    } else {
      let data1={"bdate":bdate,"btime":btime,"user_id":userid};
      let data=Object.assign(data1,booking);
       addBooking(data)
        .then(results => {
          navigation.navigate("Confirm")
        })
        .catch(error => {
          console.warn('Error');
        });
      }    
  }

  return (
    <View style={{flex: 1, padding: 20, backgroundColor: 'white'}}>
      <Text style={styles.text1}>Select Date Of Services</Text>
      <LinearGradient
        style={{width: 200, height: 70}}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={['#0873BA', '#637D77', '#CC8829']}>
        <Text style={styles.text1}>{`${month} ${year}`}</Text>
      </LinearGradient>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {date1.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{
                  borderWidth: 1.5,
                  backgroundColor: '#F3F3F3',
                  margin: 5,
                  borderRadius: 5,
                  width: 100,
                  height: 65,
                  elevation: 5,
                }} onPress={()=>handleDate(item)}>
                <Text
                  style={{fontSize: 15, textAlign: 'center', marginTop: 10}}>
                  {item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>

      <Text style={styles.text1}>Time of booking</Text>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {array.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{
                  borderWidth: 1.5,
                  backgroundColor: '#F3F3F3',
                  margin: 5,
                  borderRadius: 5,
                  width: 100,
                  height: 45,
                  elevation: 5,
                }}  onPress={()=>handleTime(item)}>
                <Text
                  style={{fontSize: 15, textAlign: 'center', marginTop: 10}}>
                  {item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>

      <TouchableOpacity
        style={{
          height: '7%',
          width: '85%',
          borderRadius: 5,
          marginTop: 20,
          alignSelf: 'center',
          backgroundColor: '#0873BA',
        }} onPress={submitData}>
        <Text style={styles.text2}>PROCEED TO CHECKOUT</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BookingSlotes;

const styles = StyleSheet.create({
  text1: {
    fontSize: 20,
    color: 'black',
    fontFamily: 'Poppins-Regular',
    fontWeight: 'bold',
    margin: 20,
  },

  text2: {
    fontSize: 15,
    color: 'white',
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    marginTop: 15,
  },

  textinput: {
    borderRadius: 5,
    height: '7%',
    width: '85%',
    alignSelf: 'center',
    marginTop: 20,
    backgroundColor: 'white',
  },
});
