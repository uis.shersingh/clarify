import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect, useRef} from 'react';
import {Searchbar} from 'react-native-paper';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import SliderBoxHome from '../sliderboxhome';
import HomeScreenscrollone from '../HomeScreenscrollone';
import DropdownComponent from '../Dropdown';
import LinearGradient from 'react-native-linear-gradient';
import {catList, SubCatList} from '../Api/Api';
import AUTH_CONFIG from '../Constant/Constant';
import {useNavigation} from '@react-navigation/native';

const HomeTab = () => {
  const [catData, setCatData] = useState();
  const scrollRef = useRef();
  useEffect(() => {
    SubCatList(0)
      .then(results => {
        setCatData(results.data);
      })
      .catch(error => {
        console.warn('Error');
      });
  }, []);

  const navigation = useNavigation();

  return (
    <>
      <View style={styles.Container}>
        <ScrollView
          ref={scrollRef}
          onContentSizeChange={() => {
            scrollRef.current.scrollTo({x: 0, y: 0, animated: true});
          }}>
          <View style={{flexDirection: 'row', marginTop: 10}}>
            <View style={{flex: 1}}>
              <DropdownComponent />
            </View>
          </View>

          <Searchbar style={{margin: 10, marginTop: 10}} />
          <View style={styles.textInputcontainer}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#0873BA', '#637D77', '#CC8829']}>
              <Text style={styles.textInputtwo}>
                {`Become a member and save 15% EXTRA  >`}
              </Text>
            </LinearGradient>
          </View>

          <View style={styles.gridgroup}>
            {catData &&
              catData.map((item, index) => {
                if (item.parent_cat != null) {
                  let img_url = `${AUTH_CONFIG.BASE_URL}/public/assets/images/upload/category/${item.image}`;

                  return (
                    <View style={styles.gridItem} key={index}>
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate('ServiceCategory', {
                            id: item.id,
                            title: item.title,
                          });
                        }}>
                        <View style={styles.gridimage}>
                          <Image
                            style={styles.img}
                            resizeMode="cover"
                            source={{uri: img_url}}
                          />
                        </View>
                        <Text style={styles.text1}>{item.title}</Text>
                      </TouchableOpacity>
                    </View>
                  );
                }
              })}
       
          </View>
        </ScrollView>
      </View>
    </>
  );
};

export default HomeTab;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white',
    paddingBottom: 20,
  },
  textone: {
    fontSize: 13,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Poppins',
    fontWeight: '400',
  },
  textInputcontainer: {
    padding: 5,
    borderRadius: 10,
  },
  Inputboxone: {
    borderWidth: 1,
    borderColor: 'black',
    // padding:8,
  },
  textInputtwo: {
    color: 'white',
    fontSize: 15,
    padding: 10,
    borderRadius: 5,
    margin: 5,
  },
  gridItem: {
    margin: '4%',
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 20,
  },
  img: {
    width: 60,
    height: 60,
  },
  gridgroup: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  gridimage: {
    padding: 10,
    borderWidth: 1.3,
    borderColor: 'gray',
    shadowColor: '#171717',
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
  },

  text1: {
    fontSize: 13,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Poppins',
    fontWeight: '400',
  },
});
