import {View, Text, StyleSheet, SafeAreaView} from 'react-native';
import React, {useState, useEffect} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';

const BookingConfirm = () => {
  const navigation = useNavigation();
  useEffect(()=>{
    setTimeout(()=>{
      navigation.navigate("BottomTab")
    },2000)
  })
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text
          style={{
            fontSize: 22,
            fontWeight: 'bold',
            color: 'black',
            alignSelf: 'center',
          }}>
          Booking Confirmed
        </Text>

        <AntDesign
          name="checkcircle"
          size={45}
          color="green"
          style={{marginTop: 20, alignSelf: 'center'}}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,

    justifyContent: 'center',
    backgroundColor: 'white',
  },
  mainContainer: {
    padding: 10,
    flexDirection: 'row',
  },
});

export default BookingConfirm;
