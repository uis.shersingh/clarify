import * as React from 'react';

import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  SafeAreaView,
  Button,
  TouchableOpacity,
  ImageBackground,
  useWindowDimensions,
} from 'react-native';

import {TabView, SceneMap} from 'react-native-tab-view';
import Armstab from './TopTabSaloon/Armstab';
import LegsTab from './TopTabSaloon/Legs';
import FullBody from './TopTabSaloon/FullBody';

const renderScene = SceneMap({
  first: Armstab,
  second: LegsTab,
  third: FullBody,
});

export default function SaloonTabView() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Arms'},
    {key: 'second', title: 'Legs'},
    {key: 'third', title: 'FullBody'},
  ]);

  return (
    <View style={{flex: 1}}>
      <ImageBackground
        source={require('../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 6}}>
            <Text style={styles.HeaderTitile}>Saloon Services</Text>
          </View>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/sidebar.png')}
              style={styles.imgheader2}
            />
          </View>
        </View>
      </ImageBackground>

      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  title1: {
    fontFamily: 'Poppins',
    fontWeight: 'bold',
    fontStyle: 'normal',
    fontSize: 16,
    marginTop: 10,
    color: '#000000',
  },

  title2: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 14,
    color: '#7B6F72',
  },

  title3: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 14,
    color: '#7B6F72',
    textAlign: 'center',
    marginTop: 10,
  },
  img: {
    alignSelf: 'center',
    marginTop: 15,
  },

  img1: {
    alignSelf: 'center',
    marginTop: 45,
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {
    alignSelf: 'center',
    marginTop: 30,
  },

  imgheader2: {
    alignSelf: 'center',
    marginTop: 30,
    marginRight: 10,
  },
});
