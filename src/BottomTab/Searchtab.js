import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {Searchbar} from 'react-native-paper';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {catList} from '../Api/Api';
import AUTH_CONFIG from '../Constant/Constant';
import {useNavigation} from '@react-navigation/native';
import DropdownComponent from '../Dropdown';

const SearchTab = () => {
  const [catData, setCatData] = useState();
  useEffect(() => {
    catList()
      .then(results => {
        setCatData(results.data);
      })
      .catch(error => {
        console.warn('Error');
      });
  });

  const navigation = useNavigation();
  return (
    <>
      <View style={styles.Container}>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 1}}>
            <DropdownComponent />
          </View>
        </View>

        <Searchbar style={{margin: 10, marginTop: 20}} />

        <Text style={styles.textInputtwo}>All categories</Text>
<ScrollView > 
        <View style={styles.gridgroup}>
          {catData &&
            catData.map((item, index) => {
              if (item.parent_cat != null) {
                let img_url = `${AUTH_CONFIG.BASE_URL}/public/assets/images/upload/category/${item.image}`;

                return (
                  <View style={styles.gridItem} key={index}>
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('ServiceCategory', {
                          id: item.id,
                        });
                      }}>
                      <View style={styles.gridimage}>
                        <Image
                          style={styles.img}
                          resizeMode="cover"
                          source={{uri: img_url}}
                        />
                      </View>
                      <Text style={styles.text1}>{item.title}</Text>
                    </TouchableOpacity>
                  </View>
                );
              }
            })}

        </View>

        </ScrollView>
      </View>
    </>
  );
};

export default SearchTab;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white',
  },
  textone: {
    fontSize: 13,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Poppins',
    fontWeight: '400',
  },
  textInputcontainer: {
    padding: 5,
  },
  Inputboxone: {
    borderWidth: 1,
    borderColor: 'black',
    // padding:8,
  },
  textInputtwo: {
    color: 'black',
    fontSize: 20,
    padding: 10,
    borderRadius: 5,
    margin: 5,
    fontFamily: 'Poppins',
    marginLeft: 10,
    fontWeight: 'bold',
  },
  gridItem: {
    margin: '4%',
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 20,
  },
  img: {
    width: 60,
    height: 60,
  },
  gridgroup: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  gridimage: {
    padding: 10,
    borderWidth: 1.3,
    borderColor: 'gray',
    shadowColor: '#171717',
    borderRadius: 10,
    backgroundColor: '#F3F3F3',
  },

  text1: {
    fontSize: 13,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Poppins',
    fontWeight: '400',
  },
});
