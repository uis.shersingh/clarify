import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import { useNavigation } from '@react-navigation/native';

const Slider2= () => {

  const navigation = useNavigation();
  useEffect(() => {
    setTimeout(() => {
      // go to Home page
      navigation.navigate('Slider3');
    }, 1000);
  }, []);

  return (
    <ImageBackground
      source={require('../../assets/img/slider2.png')}
      resizeMode="cover"
      style={{
        flex: 1,
        alignSelf: 'center',
      width:"100%"
      }}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.3)',
          position: 'relative',
        }}>
        {/* <View style={{ flex:1,backgroundColor:"black" , opacity:0.3,zIndex:99}}></View> */}
        <View style={{flex: 0.55}}>
  
        </View>
        <View style={{flex: 0.3}}>
          <Text style={styles.title1}>Painting Services</Text>
          <Text style={styles.title2}>
          All kinds of painting jobs with our experts {"\n"}
with end to end management.
          </Text>
        </View>

        {/* <Image    source={require('../../assets/img/Button.png' )} resizeMode="cover" style={{  alignSelf:"flex-end"}} /> */}
        <Image
          style={{position: 'absolute', bottom: 0}}
          source={require('../../assets/img/Vector1.png')}
          resizeMode="cover"
        />
      </View>
    </ImageBackground>
  );
};

export default Slider2;

const styles = StyleSheet.create({
  title1: {
    fontFamily: 'Poppins-ExtraBold',
    fontWeight: '800',
    fontSize: 22,
    color: '#000000',
    alignSelf: 'center',
    color: 'white',
  },

  title2: {
    fontFamily: 'Poppins-SemiBold',
    fontWeight: '600',
    alignSelf: 'center',
    fontSize: 14,
    color: '#7B6F72',
    marginTop: 8,
    color: 'white',
  },
});

