import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Button,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useState, useEffect} from 'react';

import {useNavigation,useRoute} from '@react-navigation/native';
import {catList,SubCatList} from './Api/Api';
import AUTH_CONFIG from './Constant/Constant';

const ServiceCategory = () => {
  const navigation = useNavigation();
  const [data, setData] = useState();
  const route=useRoute();
  let title=route?route.params.title:"Category";

  useEffect(() => {
    if(route.params.id){
      let id=route.params.id;
      SubCatList(id)
      .then(results => {
        setData(results.data);
      })
      .catch(error => {
        console.warn('Error');
      });
    } else {
    catList()
      .then(results => {
        setData(results.data);
      })
      .catch(error => {
        console.warn('Error');
      });
    }
  });
  
  
  return (
    <View>
      
        <ImageBackground
        source={require('../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 6}}>
            <Text style={styles.HeaderTitile}>{title}</Text>
          </View>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/sidebar.png')}
              style={styles.imgheader2}
            />
          </View>
        </View>
      </ImageBackground>
      <ScrollView >  
      {/* <TouchableOpacity  onPress={ ()=>navigation.navigate("AcServices")   } >   */}
      <FlatList
        data={data}
        renderItem={ServiceData => {
          let img_url = `${AUTH_CONFIG.BASE_URL}public/assets/images/upload/category/${ServiceData.item.image}`;
          return (
            <View style={styles.container}>
              <View style={styles.mainContainer}>
              <TouchableOpacity onPress={() => {
                  navigation.navigate('AcServices', {
                    id: ServiceData.item.id,
                    title: ServiceData.item.title,
                  });
                }}>
                <View style={{flex: 5, flexDirection: 'row'}}>
                  <View>
                    <Image source={{uri: img_url}} style={styles.img} />
                  </View>
                  <View style={{margin: 10}}>
                    <Text style={styles.textHeading}>
                      {ServiceData.item.title}
                    
                    </Text>
                    <Text style={styles.textHeadingtwo}>
                      {ServiceData.item.description}
                    </Text>
                  </View>
                </View>
                </TouchableOpacity>
              </View>
            </View>
          );
        }}
      />
      </ScrollView>
      {/* </TouchableOpacity> */}
    </View>
  );
};

export default ServiceCategory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
  },
  mainContainer: {
    padding: 10,
    flexDirection: 'row',

    borderColor: 'gray',
    borderWidth: 1.3,

    borderRadius: 10,
  },
  btn: {
    marginTop: 10,
    backgroundColor: '#0873BA',
    height: 40,
    width: 70,
    borderRadius: 5,
    fontFamily: 'Poppins',
  },
  text1: {
    margin: 5,
  },
  btntext: {
    alignSelf: 'center',
    color: 'white',
    marginTop: 10,
    fontSize: 13,
    fontFamily: 'Poppins',
  },
  img: {
    height: 65,
    width: 65,
  },
  textHeading: {
    color: 'black',
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    color: '#000000',
  },
  textHeadingtwo: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 24,
    /* identical to box height */
    color: '#7B6F72',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 30},
  imgheader2: {alignSelf: 'flex-start', marginTop: 30, marginRight: 20},
});
