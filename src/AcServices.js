import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Button,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Modal,
  Pressable,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {ServiceByCat} from './Api/Api';
import AUTH_CONFIG from './Constant/Constant';
import FrequenltyItem from './BottomTab/FrequentlyItem';

const AcServices = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();
  const [data, setData] = useState();
  const [modaldata, setModalData] = useState({id:"",title:"",price:"",description:"",qty:1});
  const route = useRoute();
  let title = route ? route.params.title : '';

  useEffect(props => {
    if (route.params.id) {
      let id = route.params.id;
      ServiceByCat(id)
        .then(results => {
          setData(results.data);
        })
        .catch(error => {
          console.warn('Error');
        });
    }
  });
 
  const showModal=(id,title,price,description)=>{
    setModalVisible(true)
    setModalData({
      id:id,
      title:title,
      price:price,
      description:description
    })
  }

  const handleBook=()=>{
    navigation.navigate("Address",{bookingData:modaldata})
  }

  const handleIncDec=(inc)=>{
    console.warn(inc)
    let qty=1;
    if(inc==="Inc"){
      qty=modaldata.qty+1;
      setModalData({qty:qty})
    }
    if(inc==="Dec"){
      qty=modaldata.qty-1;
      setModalData({qty:qty})
    }
  }
  
  return (
    <View>
      
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Pressable onPress={() => setModalVisible(!modalVisible)}>
              <Image
                source={require('../assets/img/closeicon.png')}
                style={styles.closeicon}
              />
            </Pressable>
            <Text style={styles.text22}>{modaldata.title}</Text>
            <Text style={styles.text22}> Details </Text>
            <Text style={styles.text222}>
              {modaldata.description}
            </Text>

            <View style={styles.additemcard}>
              <View style={{flex: 3, borderRightWidth: 1}}>
                <TouchableOpacity onPress={()=>handleIncDec('Inc')}>
                  <Text style={styles.text2222}>-</Text>
                </TouchableOpacity>
              </View>

              <View style={{flex: 3, borderRightWidth: 1}}>
                <TouchableOpacity>
                  <Text style={styles.text2222}> 1 </Text>
                </TouchableOpacity>
              </View>

              <View style={{flex: 3}}>
                <TouchableOpacity onPress={()=>handleIncDec('Dec')}>
                  <Text style={styles.text2222}> +</Text>
                </TouchableOpacity>
              </View>
            </View>
            <FrequenltyItem />
            <View style={styles.booknowcard}>
              <View style={{flex: 3}}>
                <Text style={styles.text333}>1 Item Added</Text>
              </View>

              <View style={{flex: 3}}>
                <Text style={styles.text33}> Rs. {modaldata.price} </Text>
              </View>

              <View style={{flex: 3}}>
                <TouchableOpacity style={styles.btn1} onPress={handleBook}>
                  <Text style={styles.text3}>Book Now</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal> 

      <ImageBackground
        source={require('../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 6}}>
            <Text style={styles.HeaderTitile}>{title}</Text>
          </View>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/sidebar.png')}
              style={styles.imgheader2}
            />
          </View>
        </View>
      </ImageBackground>
      <FlatList
        data={data}
        renderItem={ServiceData => {
          let img_url = `${AUTH_CONFIG.BASE_URL}public/assets/images/upload/service/${ServiceData.item.image}`;
          const {id,title,price,price_type,min_price,max_price,description}=ServiceData.item
          return (
            <View style={styles.container}>
              <View style={styles.mainContainer}>
                <View style={{flex: 2, flexDirection: 'row'}}>
                  <Image source={{uri: img_url}} style={styles.img} />

                  <View style={{margin: 10, flex: 6}}>
                    <Text style={styles.textHeading}>
                      {title}
                    </Text>
                    <Text style={styles.textHeadingtwo}>
                      Rs.{' '}
                      {price_type === 'range'
                        ? min_price +
                          ' - ' +
                          max_price
                        : price}
                    </Text>
                  </View>
                  <View style={{flex: 2}}>
                    <TouchableOpacity
                      style={styles.btn}
                      onPress={() => showModal(id,title,price,description)}>
                      <Text style={styles.btntext}> ADD</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

export default AcServices;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  mainContainer: {
    padding: 10,
    flexDirection: 'row',
    // borderWidth:1,
    // borderColor:"#D3D3D3",
    // backgroundColor:"#F3F3F3",
    // elevation:5,
    // borderRadius:10
  },
  btn: {
    marginTop: 10,
    backgroundColor: '#0873BA',
    height: 40,
    width: 70,
    borderRadius: 5,
    fontFamily: 'Poppins',
  },
  text1: {
    margin: 5,
  },
  btntext: {
    alignSelf: 'center',
    color: 'white',
    marginTop: 10,
    fontSize: 13,
    fontFamily: 'Poppins',
  },
  img: {
    height: 65,
    width: 65,
  },
  textHeading: {
    color: 'black',
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    color: '#000000',
  },
  textHeadingtwo: {
    fontFamily: 'Poppins',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 24,
    /* identical to box height */
    color: '#7B6F72',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 30},
  imgheader2: {alignSelf: 'flex-start', marginTop: 30, marginRight: 20},

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: 'rgba(0, 0, 0, .7)',
  },
  modalView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    height: '80%',
    width: '90%',
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.7,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  closeicon: {
    alignSelf: 'flex-end',
    marginLeft: '100%',
  },

  text22: {
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
  },

  text222: {
    fontSize: 15,
    paddingTop: 10,
    alignSelf: 'flex-start',
  },

  text2222: {
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
    padding: 5,
  },

  additemcard: {
    flexDirection: 'row',
    Height: 30,
    width: 140,
    borderWidth: 1,
    padding: 5,
    borderColor: 'gray',
    marginTop: 20,
    alignSelf: 'flex-start',
  },

  booknowcard: {
    flexDirection: 'row',
    Height: 30,
    width: '100%',

    padding: 5,

    marginTop: 20,
  },

  btn1: {
    marginTop: 10,
    backgroundColor: '#0873BA',
    height: 50,
    width: 120,
    borderRadius: 5,
    fontFamily: 'Poppins',
  },

  text3: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 15,
  },

  text33: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },

  text333: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 14,
    backgroundColor: '#D9D9D9',
    padding: 4,
  },
});
