import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import {useNavigation} from '@react-navigation/native';
import {customerRegister} from "./Api/Api"
const LoginScreen1 = () => {
  const navigation = useNavigation();
  const [error, setError] = React.useState({});
  const [email, setEmail] = React.useState("");
  const [pass, setPass] = React.useState();
  const [inputs, setInputs] = React.useState({});

  const handleValidate=()=>{
      let errors = {};
      let formIsValid = true;
  
      
      if(!inputs.name){
        formIsValid = false;
        errors["name"] = "Please type name";
      }
      
      if(!inputs.email){
         formIsValid = false;
         errors["email"] = "Please type email";
      }
        //validating email
      if(typeof inputs.email !== "undefined"){
         let lastAtPos = inputs.email.lastIndexOf('@');
         let lastDotPos = inputs.email.lastIndexOf('.');
  
         if (!(lastAtPos < lastDotPos && lastAtPos > 0
         && inputs.email.indexOf('@@') == -1 && 
         lastDotPos > 2 && (inputs.email.length - lastDotPos) > 2)) {
            formIsValid = false;
            errors["email"] = "Email is not valid";
          }
     }  

      if(!inputs.password){
        formIsValid = false;
        errors["password"] = "Please type password";
      }
      if(inputs.password.length<6){
        formIsValid = false;
        errors["password"] = "Please type password min 6 character";
      }
      if(inputs.password!==inputs.cpassword){
        formIsValid = false;
        errors["password"] = "Password does not match";
      }

      if(!inputs.phone){
        formIsValid = false;
        errors["phone"] = "Please type mobile";
      }
      // if(inputs.phone.length<10){
      //   formIsValid = false;
      //   errors["phone"] = "Please type correct mobile no";
      // }
      if(!inputs.age){
        formIsValid = false;
        errors["age"] = "Please type age";
      }
 
     setError(errors);
     return formIsValid;
  }

 const handleClick=()=>{
    if(handleValidate()){
      customerRegister(inputs)
      .then(results => {
        if(!results.error){
          console.log("results",results.data)
          console.warn('Register Success');
          navigation.navigate("OTP1",{register:results.data})
        } else {
          let errors={};
          errors["error"] = results.data;
          setError(errors);
        }
        
      })
      .catch(error => {
        console.warn('Error',error.data);
      });
  }
  }
  const handleChange=(e,input)=>{
    setInputs({...inputs,[input]: e})

    console.log("inputs",inputs)
  }
  return (
    <View style={{flex: 1 , }}>
      <ImageBackground
        source={require('../assets/img/Loginimage.jpeg')}
        style={{height: '100%', width: '100%' , }}>
        <Text style={styles.text1}> Let’s get started</Text>
        {error.error && (
          <Text style={{color:"red",marginLeft:30}}>{error.error}</Text>
           )}
        <TextInput
          placeholder="Full Name"
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'name')}
        />
        {error.name && (
          <Text style={{color:"red",marginLeft:30}}>{error.name}</Text>
        )}
        <TextInput
          placeholder="Email "
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'email')}
        />
        {error.email && (
          <Text style={{color:"red",marginLeft:30}}>{error.email}</Text>
        )}
        <TextInput
          placeholder="Password"
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'password')}
        />
        {error.password && (
          <Text style={{color:"red",marginLeft:30}}>{error.password}</Text>
        )}

        <TextInput
          placeholder="Re-Entered Password"
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'cpassword')}
        />
        {error.cpassword && (
          <Text style={{color:"red",marginLeft:30}}>{error.cpassword}</Text>
        )}

        <TextInput
          placeholder="Mobile No."
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'phone')}
        />
        {error.phone && (
          <Text style={{color:"red",marginLeft:30}}>{error.phone}</Text>
        )}

        <TextInput
          placeholder="Age"
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'age')}
        />
        {error.age && (
          <Text style={{color:"red",marginLeft:30}}>{error.age}</Text>
        )}

        <TextInput
          placeholder="Gender"
          placeholderTextColor="gray"
          style={styles.textinput}
          onChangeText={(e) => handleChange(e,'gender')}
        />
        {error.gender && (
          <Text style={{color:"red",marginLeft:30}}>{error.gender}</Text>
        )}

        <TouchableOpacity
          style={{
            height: '7%',
            width: '85%',
            borderRadius: 5,
            marginTop: 20,
            alignSelf: 'center',
            backgroundColor: '#0873BA',
          }}
          onPress={handleClick}>
          <Text style={styles.text2}>signup</Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default LoginScreen1;

const styles = StyleSheet.create({
  text1: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    marginTop: 100,
  },

  text2: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    marginTop: 14,
  },

  textinput: {
    borderRadius: 5,
    height: '7%',
    width: '85%',
    alignSelf: 'center',
   
    marginTop:20,
  
    backgroundColor: 'white',
  },
});
