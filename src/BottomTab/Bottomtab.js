import React,{useEffect} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeTab from './Hometab';
import NotificationTab from './Notificationtab';
import SearchTab from './Searchtab';
import UserTab from './Userprofiletab';
import BookingTab from './Bookingtab';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { current_user_id } from '../Common';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { current_user_data } from '../Common';
const Tab = createBottomTabNavigator();

function BottomTab() {
  
  return (
    <Tab.Navigator initialRouteName="HomeTab">
      <Tab.Screen
        name="NotificationTab"
        component={NotificationTab}
        options={{
          headerShown: false,
          tabBarLabel: () => {
            return null;
          },
          tabBarActiveTintColor: '#0873BA',
          tabBarInactiveTintColor: '#ADA4A5',
          tabBarIcon: ({color, size}) => (
            <Ionicons name="notifications" size={30} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="BookingTab"
        component={BookingTab}
        options={{
          headerShown: false,
          tabBarActiveTintColor: '#0873BA',
          tabBarInactiveTintColor: '#ADA4A5',
          tabBarLabel: () => {
            return null;
          },
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="clipboard-list"
              size={30}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="HomeTab"
        component={HomeTab}
        options={{
          headerShown: false,

          tabBarActiveTintColor: '#0873BA',
          tabBarInactiveTintColor: '#ADA4A5',
          tabBarLabel: () => {
            return null;
          },
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" size={30} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="SearchTab"
        component={SearchTab}
        options={{
          headerShown: false,
          tabBarActiveTintColor: '#0873BA',
          tabBarInactiveTintColor: '#ADA4A5',
          tabBarLabel: () => {
            return null;
          },
          tabBarIcon: ({color, size}) => (
            <FontAwesome name="search" size={30} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="UserTab"
        component={UserTab}
        options={{
          headerShown: false,
          tabBarActiveTintColor: '#0873BA',
          tabBarInactiveTintColor: '#ADA4A5',
          tabBarLabel: () => {
            return null;
          },
          tabBarIcon: ({color, size}) => (
            <FontAwesome name="user" size={30} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTab;
