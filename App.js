import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './src/Home';
import Profile from './src/Profile';
import BottomTab from './src/BottomTab/Bottomtab';
import Slider1 from './src/Slider/Slider1';
import Slider2 from './src/Slider/Slider2';
import Slider3 from './src/Slider/Slider3';
import Slider4 from './src/Slider/Slider4';
import Slider5 from './src/Slider/Slider5';
import WelcomeScreen from './src/WelcomeScreen';
import SplashScreen from './src/Splashscreen';
import LoginScreen from './src/Login';
import LoginScreen1 from './src/Login1';
import OTP1 from './src/Otp1';
import OTP2 from './src/Otp2';
import ProfileEdit from './src/EditProfile';
import SliderBoxHome from './src/sliderboxhome';
import HomeScreenscrollone from './src/HomeScreenscrollone';
import AcServices from './src/AcServices';
import ServiceCategory from './src/ServiceCategory';
import DropdownComponent from './src/Dropdown';
import SaloonTabView from './src/Saloontabview';
import BookingSlotes from './src/BookingSlotes';
import FrequenltyItem from './src/BottomTab/FrequentlyItem';
import BookingTab from './src/BottomTab/Bookingtab';
import SlectAddress from './src/SlectAddress';
import BookingConfirm from './src/BookingConfirm'

const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="WelcomeScreen"
          component={WelcomeScreen}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Slider1"
          component={Slider1}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Slider2"
          component={Slider2}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Slider3"
          component={Slider3}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Slider4"
          component={Slider4}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Slider5"
          component={Slider5}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="Login1"
          component={LoginScreen1}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="OTP1"
          component={OTP1}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="OTP2"
          component={OTP2}
          options={{title: 'Welcome', headerShown: false}}
        />
        <Stack.Screen
          name="BottomTab"
          component={BottomTab}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="ProfileEdit"
          component={ProfileEdit}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="SliderBoxHome"
          component={SliderBoxHome}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="HomeScreenscrollone"
          component={HomeScreenscrollone}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="AcServices"
          component={AcServices}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="ServiceCategory"
          component={ServiceCategory}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="DropdownComponent"
          component={DropdownComponent}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="SaloonTabView"
          component={SaloonTabView}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="BookingSlotes"
          component={BookingSlotes}
          options={{title: 'Welcome', headerShown: false}}
        />

        <Stack.Screen
          name="Address"
          component={SlectAddress}
          options={{title: 'Address', headerShown: false}}
        />

        <Stack.Screen
          name="Confirm"
          component={BookingConfirm}
          options={{title: 'Address', headerShown: false}}
        />
        
       <Stack.Screen
          name="FrequenltyItem"
          component={FrequenltyItem}
          options={{title: 'Welcome', headerShown: false}}
        />
      </Stack.Navigator>

      
    </NavigationContainer>
  );
};

export default MyStack;
