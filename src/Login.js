import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {userLogin} from "./Api/Api";
import AsyncStorage from '@react-native-async-storage/async-storage'
import {current_user_id} from "./Common"

const LoginScreen = () => {
  const navigation = useNavigation();
  const [error, setError] = React.useState({});
  const [email, setEmail] = React.useState("");
  const [pass, setPass] = React.useState();

  const handleValidate=()=>{
    
      let errors = {};
      let formIsValid = true;
  
      //Name check if name is empty or not
      if(!pass){
         formIsValid = false;
         errors["pass"] = "Please type password";
      }
   
      //Email should not be empty
      if(!email){
         formIsValid = false;
         errors["email"] = "Please type email";
      }
        //validating email
      if(typeof email !== "undefined"){
         let lastAtPos = email.lastIndexOf('@');
         let lastDotPos = email.lastIndexOf('.');
  
         if (!(lastAtPos < lastDotPos && lastAtPos > 0
         && email.indexOf('@@') == -1 && 
         lastDotPos > 2 && (email.length - lastDotPos) > 2)) {
            formIsValid = false;
            errors["email"] = "Email is not valid";
          }
     }  
  
     setError(errors);
     return formIsValid;
  }
  // const checking=async ()=>{
    
  //  // console.log("Store",stringfy(AsyncStorage.getItem('user_data')));
  //  let user = await AsyncStorage.getItem('user_id');  
  //  let parsed = JSON.parse(user);  
  //  console.log("Store2",parsed);
  // }

 const handleClick=()=>{
    if(handleValidate()){
      userLogin(email,pass)
      .then(results => {
        if(!results.error){
        //  AsyncStorage.setItem('user_data', results.data); 
          AsyncStorage.setItem('user_id', results.data.user_id); 
          AsyncStorage.setItem('user_email', results.data.email); 
          navigation.navigate("BottomTab",{user_info:results.data})
        } else {
          let errors={};
          errors["error"] = results.data;
          setError(errors);
        }
        
      })
      .catch(error => {
        console.warn('Error',error.data);
      });
  }
  }

  useEffect(()=>{
    (async function (){
      const user_id=await current_user_id()
      if(user_id){
        navigation.navigate("BottomTab",{user_info:current_user_id})
      }
    })()
  
  })

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ImageBackground
        source={require('../assets/img/Loginimage.jpeg')}
        style={{height: '100%', width: '100%'}}>  
        {error.error && (
          <Text style={{color:"red",marginLeft:20}}>{error.error}</Text>
           )}
        <TextInput
          placeholder="Email"
          placeholderTextColor="gray"
          style={styles.textinput1}
          onChangeText={email => setEmail(email)}
          />
         {error.email && (
          <Text style={{color:"red",marginLeft:20}}>{error.email}</Text>
        )}
        <View>
          <TextInput
            placeholder="Password"
            placeholderTextColor="gray"
            style={styles.textinput}
            onChangeText={pass => setPass(pass)}
          />
          {error.pass && (
          <Text style={{color:"red",marginLeft:20}}>{error.pass}</Text>
           )}
        </View>
        
        <TouchableOpacity
          style={{
            height: '7%',
            width: '90%',
            borderRadius: 5,
            marginTop: -100,
            alignSelf: 'center',
            backgroundColor: '#0873BA',
          }}
          onPress={handleClick}>
          <Text style={styles.text2}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: '7%',
            alignSelf: 'center',
          }}
          onPress={() =>navigation.navigate("Login1")}
          >
          <Text style={styles.text2}>Register</Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  text1: {
    fontSize: 18,
    color: 'white',
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    marginTop: 350,
  },

  text2: {
    fontSize: 15,
    color: 'white',
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    marginTop: 15,
  },

  textinput1: {
    borderRadius: 5,
    height: '7%',
    width: '90%',
    alignSelf: 'center',
    marginTop: 350,
    backgroundColor: 'white',
  },

  textinput: {
    borderRadius: 5,
    height: '25%',
    width: '90%',
    alignSelf: 'center',
    marginTop: 20,
    backgroundColor: 'white',
  },
});
