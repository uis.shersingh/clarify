import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useNavigation, useRoute} from '@react-navigation/native';
import {updateOtp} from "./Api/Api"

const OTP1 = (props) => {
  const navigation = useNavigation();
  const route = useRoute();
  let register = route ? route.params : '';

  const email="uis.shersingh@gmail.com"

    const {initialMinute = 0,initialSeconds = 30} = props;
    const [ minutes, setMinutes ] = useState(initialMinute);
    const [seconds, setSeconds ] =  useState(initialSeconds);
    useEffect(()=>{
      let myInterval = setInterval(() => {
              if (seconds > 0) {
                  setSeconds(seconds - 1);
              }
              if (seconds === 0) {
                  // if (minutes === 0) {
                  //     clearInterval(myInterval)
                  // } else {
                  //     setMinutes(minutes - 1);
                  //     setSeconds(30);
                  // }
                  updateOtp(email)
                    .then(results => {
                      //console.warn('Opt resend',results.data);
                    }).catch(error => {
                      //console.warn('Error',error.data);
                    });
                  setSeconds(30);
              } 
          }, 1000)
        return ()=> {
            clearInterval(myInterval);
          };
    });
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ImageBackground
        source={require('../assets/img/background.png')}
        style={{height: 80, width: 400}}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 2}}>
            <Image
              source={require('../assets/img/arrow.png')}
              style={styles.imgheader}
            />
          </View>
          <View style={{flex: 8}}>
            <Text style={styles.HeaderTitile}>Login/signup</Text>
          </View>
        </View>
      </ImageBackground>

      <Text style={styles.title1}>Enter verification code</Text>
      <Text style={styles.title2}>
        We have sent you a 4 digit verification code on
      </Text>
      <Text style={styles.title3}>{email}</Text>

      <View
        style={{
          height: hp('7%'),
          width: wp('85% '),
          justifyContent: 'center',
          alignSelf: 'center',
          flexDirection: 'row',

          marginTop: hp('5%'),
        }}>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
        <View style={{flex: 0.5}}>
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            maxFontSizeMultiplier={1}
            style={styles.TextInput}></TextInput>
        </View>
      </View>
      <Text style={styles.title4}>{minutes}:{seconds}</Text>

      <TouchableOpacity style={styles.btncontainer}
      onPress={() => navigation.navigate('OTP2')}>
        <Text style={styles.title5}>Login/Signup</Text>
      </TouchableOpacity>
    </View>
  );
};

export default OTP1;

const styles = StyleSheet.create({
  title1: {
    fontFamily: 'Poppins',
    fontWeight: '600',
    fontStyle: 'normal',
    fontSize: 22,
    marginTop: 35,
    color: '#000000',
    alignSelf: 'center',
  },

  title2: {
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    alignSelf: 'center',
    fontSize: 14,
    color: '#7B6F72',
    marginTop: 8,
  },

  title3: {
    fontFamily: 'Poppins-Medium',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 18,
    marginTop: 20,
    color: '#000000',
    alignSelf: 'center',
  },

  title4: {
    fontFamily: 'Poppins-Regular',
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 18,
    marginTop: 4,
    color: '#F0350C',
    alignSelf: 'center',
  },

  title5: {
    fontFamily: 'Poppins-SemiBold',
    fontWeight: '600',

    fontSize: 16,
    marginTop: 10,
    color: '#0873BA',
    alignSelf: 'center',
  },

  btncontainer: {
    height: 50,
    width: 300,
    borderColor: '#0873BA',
    borderWidth: 1.3,
    alignSelf: 'center',
    marginTop: 30,
    borderRadius: 5,
  },

  maincontainer: {
    flex: 1,
    backgroundColor: 'white',
  },

  HeaderTitile: {
    color: 'white',
    fontWeight: '600',
    fontSize: 16,
    alignSelf: 'flex-start',
    marginTop: 30,
    fontFamily: 'Poppins',
  },
  imgheader: {alignSelf: 'center', marginTop: 32},

  TextInput: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderWidth: 1.3,
    borderColor: 'gray',
    alignSelf: 'center',
    borderRadius: 5,
  },
});
