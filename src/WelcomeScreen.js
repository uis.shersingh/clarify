import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';

const WelcomeScreen = () => {
  const navigation = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      // go to Home page
      navigation.navigate('Slider1');
    }, 1000);
  }, []);
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Image
        source={require('../assets/img/carifylogo.png')}
        style={{alignSelf: 'center', marginTop: 200}}
      />
      <Text style={styles.text1}> All-in-one home service provider</Text>
    </View>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  text1: {
    fontSize: 18,
    color: '#7B6F72',
    fontFamily: 'Poppins-MediumItalic',
    textAlign: 'center',
    marginTop: -40,
    fontWeight: '500',
  },
});
